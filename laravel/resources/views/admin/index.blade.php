<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Admin Dashboard</title>
    <link rel="stylesheet" href="{{ asset('css/adminlte.css') }}">
</head>
<body class="hold-transition sidebar-mini">
    <div class="wrapper">
        <!-- Content Wrapper -->
        <div class="content-wrapper">
            <!-- Main content -->
            <section class="content">
                <!-- Your content here -->
                <h1>Welcome to Admin Dashboard</h1>
            </section>
        </div>
    </div>
    <script src="{{ asset('js/adminlte.js') }}"></script>
</body>
</html>
