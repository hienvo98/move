const path = require('path');

module.exports = {
    outputDir: path.resolve(__dirname, '../laravel/public'),
    publicPath: process.env.NODE_ENV === 'production' ? '/' : '/',
    configureWebpack: {
        output: {
            filename: 'js/[name].js',
            chunkFilename: 'js/[name].js',
        }
    },
    css: {
        extract: {
            filename: 'css/[name].css',
            chunkFilename: 'css/[name].css',
        },
    }
};
